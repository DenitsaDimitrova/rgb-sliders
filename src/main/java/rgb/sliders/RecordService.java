package rgb.sliders;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class RecordService {
    private final ApplicationEventPublisher publisher; // <1>
    private final RGBRepository rgbRepository; // <2>

    RecordService(ApplicationEventPublisher publisher, RGBRepository rgbRepository) {
        this.publisher = publisher;
        this.rgbRepository = rgbRepository;
    }

    public Mono<RGBModel> create(int red, int green, int blue, String hex) { // <7>
        return Mono.just(this.rgbRepository
                .save(new RGBModel(red, green, blue, hex)))
                /* return this.rgbRepository
                         .save(new RGBModel(red,green,blue))*/

                .doOnSuccess(record -> this.publisher.publishEvent(new RecordCreatedEvent(record)));
    }
}
