package rgb.sliders;

import org.springframework.context.ApplicationEvent;

public class RecordCreatedEvent extends ApplicationEvent {

    public RecordCreatedEvent(RGBModel source) {
        super(source);
    }
}
