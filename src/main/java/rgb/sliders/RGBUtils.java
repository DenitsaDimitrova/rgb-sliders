package rgb.sliders;

public enum RGBUtils {
    ;

    static String convertRgbToHex(int red, int green, int blue){
        return String.format("#%02X%02X%02X", red, green, blue);
    }
}
