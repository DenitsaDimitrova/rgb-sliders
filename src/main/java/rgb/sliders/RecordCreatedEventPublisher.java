package rgb.sliders;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import reactor.core.publisher.FluxSink;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

@Component
public class RecordCreatedEventPublisher implements
        ApplicationListener<RecordCreatedEvent>, // <1>
        Consumer<FluxSink<RecordCreatedEvent>> {

    private final Executor executor;
    private final BlockingQueue<RecordCreatedEvent> queue =
            new LinkedBlockingQueue<>(); // <3>

    RecordCreatedEventPublisher(Executor executor) {
        this.executor = executor;
    }

    // <4>
    @Override
    public void onApplicationEvent(RecordCreatedEvent event) {
        this.queue.offer(event);
    }

    @Override
    public void accept(FluxSink<RecordCreatedEvent> sink) {
        this.executor.execute(() -> {
            while (true)
                try {
                    RecordCreatedEvent event = queue.take(); // <5>
                    sink.next(event); // <6>
                }
                catch (InterruptedException e) {
                    ReflectionUtils.rethrowRuntimeException(e);
                }
        });
    }
}
