package rgb.sliders;

import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

import static rgb.sliders.RGBUtils.convertRgbToHex;

@Component
public class RecordHandler {
    private final RecordService recordService;

    RecordHandler(RecordService profileService) {
        this.recordService = profileService;
    }

    Mono<ServerResponse> create(ServerRequest request) {
        Flux<RGBModel> flux = request
                .bodyToFlux(RGBModel.class)
                .flatMap(toWrite -> this.recordService.create(toWrite.getRed(), toWrite.getGreen(), toWrite.getBlue(),
                        convertRgbToHex(toWrite.getRed(), toWrite.getGreen(), toWrite.getBlue())));
        return defaultWriteResponse(flux);

    }

    private static Mono<ServerResponse> defaultWriteResponse(Publisher<RGBModel> profiles) {
        return Mono
                .from(profiles)
                .flatMap(p -> ServerResponse
                        .created(URI.create("/rgb"))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .build()
                );
    }
}
