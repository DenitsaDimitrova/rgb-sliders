package rgb.sliders;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RGBRepository extends JpaRepository<RGBModel, String> {
}
